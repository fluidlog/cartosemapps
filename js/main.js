/* *****************************
Welcome to the CartoSemapps code !
******************************** */

const CartoSemapps = function (){
  let configFilePath;
}

CartoSemapps.prototype.launchCarto = async function(configProject){
  thisCarto = this;
  const myGraph = new FluidGraph();
  myGraph.config = await myGraph.fetchConfig("assets/fluidgraph/config/config-graph-default.json");
  myGraph.customNodes = await myGraph.fetchConfig("assets/fluidgraph/config/config-nodes-default.json");
  myGraph.customEdges = await myGraph.fetchConfig("assets/fluidgraph/config/config-edges-default.json");
  myGraph.configProject = configProject;

  // Début de l'initialisation de thisGraph.config

  // Initialisation dépendante des valeurs récupérées dans les fichiers de config
  myGraph.nodeTypeIcon = {
    r : 13,
    cxClosed : 0,
    cxEdited : 0,
    cyClosed : (myGraph.customNodes.heightClosed/2)-10,
    cyEdited : (myGraph.customNodes.heightEdited/2)-10,
    xClosed : -11,
    xOpened : -11,
    xEdited : -11,
    yClosed : (myGraph.customNodes.heightClosed/2)-20,
    yEdited : (myGraph.customNodes.heightEdited/2)-20,
  }

  myGraph.nodeIndexCircle = {
    r : 10,
    cxClosed : 0,
    cyClosed : -(myGraph.customNodes.heightClosed/2)+6,
    cxEdited : 0,
    cyEdited : -(myGraph.customNodes.heightOpened/2),
    dxClosed : 0,
    dyClosed : -(myGraph.customNodes.heightClosed/2)+10,
    dxEdited : 0,
    dyEdited : -(myGraph.customNodes.heightOpened/2)+5,
  }

  myGraph.customNodesText = {
    fontSize : 14,
    FontFamily : "Helvetica Neue, Helvetica, Arial, sans-serif;",
    strokeOpacity : .5,
    widthMax : 160,
    heightMax : 60,
    curvesCorners : myGraph.customNodes.curvesCornersOpenedNode,
  }

  myGraph.customNodes.transitionEasing = d3.easeElastic;

  myGraph.diagonal = d3.linkHorizontal(); // changed on D3 V4.
//  myGraph.panBgLRBoundary = myGraph.svgContainer.width / myGraph.panBgBoundaryRate;
//  myGraph.panBgUDRBoundary = myGraph.svgContainer.height / myGraph.panBgBoundaryRate;

  // Fin de l'initialisation de myGraph.config

  ( async () => {
    // Par défaut, on lance la config "Home", s'il y a un hash, celui-ci le surcharge, sauf s'il est erronné (on revient à home).
    if (!myGraph.configProject && window.location.hash)
    {
      // myGraph.ldpGraphName = https://ldp.virtual-assembly.org:8443/2013/cartopair/2a1499b5dc
      let hash = window.location.hash;
      let configUriHash = hash.split("#")[1];
      if (configUriHash.includes("http"))
      {
        // if hash contain configFilePath
        // as #https://fluidlog.gitlab.io/cartosemapps/config/config-archipel.json
        myGraph.configProject = await myGraph.fetchConfig(configUriHash);
      }
      else{
        // if hash contain keyword project
        // as #archipel
        myGraph.configProject = await myGraph.fetchConfig("config/config-"+configUriHash+".json");
      }
    }
    else { // no hash -> home config
      myGraph.configProject = await myGraph.fetchConfig(thisCarto.configFilePath);
    }

    if (myGraph.configProject)
    {
      if (myGraph.configProject.config.backupDataUri)
      {
        //load backupD3Data if problem
        myGraph.backupD3Data = await myGraph.fetchConfig(myGraph.configProject.config.backupDataUri);
      }
      /****************************
      Config project :
      #archipel : https://data.virtual-assembly.org/sparql
      #fabmob : https://data.fabmob.io/middleware/sparql
      #utt : https://data-techcico.semapps.org/sparql
      #colibris : https://colibris.social/sparql
      #resilience : https://data.resilience.data-players.com/sparql

      Config exemples :
      0.json : 1 noeud, 0 liens
      1.json : 2 noeuds, un lien
      2.json : 8 noeuds et 7 liens
      3.json : 9 noeuds, 9 liens - ontologie QQOQCCP
      4.json : qui, quoi, ou... - mais avec les couleuts de PAIR
      5.json : 2 noeuds liés - ontologie QQOQCCP
      6.json : 4 noeuds liés - ontologie QQOQCCP
      7.json : les misérables (exemple D3js) -> Ne fonctionne pas car il n'y a pas d'index -> ok en décommentant .id(d => d.label) dans updateForces()
      8.json : Exemple carto des cartos chargée à l'accueil
      */

      myGraph.config = Object.assign( {}, myGraph.config, myGraph.configProject.config );
      myGraph.customNodes = Object.assign( {}, myGraph.customNodes, myGraph.configProject.customNodes );
      myGraph.customNodes.strokeNeighbourColorType = myGraph.customNodes.colorType;
      myGraph.customNodes.strokeNeighbourColorTypeRgba = myGraph.customNodes.colorTypeRgba;
      myGraph.customNodes.strokeColorType = myGraph.customNodes.colorType;
    }
    else console.log("pas de json");

    myGraph.initSvgContainer("#chart");

    // On doit avoir la possibilité de lancer les deux étapes manuellement
    if (myGraph.config.autoLoad)
      await myGraph.chooseLoaderAndLoadD3Data();

    if (myGraph.config.autoDisplay)
      await myGraph.initializeAndDisplayGraph();

    // Ces deux fonctions doivent se charger au début, en premier, même avant le chargement des données.
    myGraph.menuInitialisation();
    await myGraph.control();
    await myGraph.initializeFiltersInMenu();

    if (myGraph.config.keepMenuOpen)
      $('.ui.sidebar').sidebar('toggle');

  })();
}
