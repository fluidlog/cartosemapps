FluidGraph.prototype.drawLinks = function(svgEdges){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("drawLinks start");

  if (thisGraph.config.curvesEdges)
  {
    svgEdges.attr("class", "link")
            // .attr("id", function(d) {
            //   let index = "edge"+d.source.index + "_" + d.target.index;
            //   return index;
            // })
            .attr("stroke", thisGraph.customEdges.strokeColor)
            .attr("stroke-width", thisGraph.customEdges.strokeWidth)
            // .attr("d", function(d) {
            //             var dx = d.target.x - d.source.x,
            //                 dy = d.target.y - d.source.y,
            //                 dr = Math.sqrt(dx * dx + dy * dy);
            //             return "M" +
            //                 d.source.x + "," +
            //                 d.source.y + "A" +
            //                 dr + "," + dr + " 0 0,1 " +
            //                 d.target.x + "," +
            //                 d.target.y;
            //           })
            .style("fill", "none")
  }
  else { //Off
    svgEdges.attr("class", "link")
            // .attr("id", function(d) {
            //   let index = "edge"+d.source.index + "_" + d.target.index;
            //   return index;
            // })
            .attr("stroke", thisGraph.customEdges.strokeColor)
            .attr("stroke-width", thisGraph.customEdges.strokeWidth)
            // .attr("x1", function(d) { return d.source.x; })
    		  	// .attr("y1", function(d) { return d.source.y; })
    		  	// .attr("x2", function(d) { return d.target.x; })
    		  	// .attr("y2", function(d) { return d.target.y; })
  }

  if (thisGraph.config.debug) console.log("drawLinks end");
}

FluidGraph.prototype.linkEdit = function(d3Edge, edgeData){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("linkEdit start");

  d3.event.stopPropagation();

  var el = d3Edge;
  var p_el = d3.select(d3Edge.node().parentNode);

  var searchLabelIndex = "#label" + edgeData.source.index + "_" + edgeData.target.index;
  d3.select(searchLabelIndex).attr("visibility", "hidden");

  //Close opened linkEditBox
  d3.selectAll("#linkEditBox").remove();
  d3.selectAll("#fo_content_edited_linklabel").remove();

  thisGraph.state.editedLinkLabel = d3Edge.node();

  var linkEditBoxX = edgeData.source.x
                    + (edgeData.target.x - edgeData.source.x)/2
                    - thisGraph.customEdges.label.width/2;
  var linkEditBoxY = edgeData.source.y
                    + (edgeData.target.y - edgeData.source.y)/2
                    -thisGraph.customEdges.label.height/2;

  p_el.append("rect")
      .attr("id", "linkEditBox")
      .attr("class", "linkEditBox")
      .attr("x", linkEditBoxX )
      .attr("y", linkEditBoxY )
      .attr("width", thisGraph.customEdges.label.width)
      .attr("height", thisGraph.customEdges.label.height)
      .attr("rx", thisGraph.customEdges.label.curvesCorners)
      .attr("ry", thisGraph.customEdges.label.curvesCorners)
      .style("fill", thisGraph.customEdges.label.fillColor)
      .style("stroke", thisGraph.customEdges.label.strokeColor)
      .style("stroke-width", 2)
      .style("stroke-opacity", .5)
      .style("cursor", thisGraph.customNodes.cursor)
      .style("opacity", .8)

  /*
   *
   * Content of the linklabel
   *
   * */

  var fo_content_edited_linklabel = p_el
        .append("foreignObject")
        .attr("id","fo_content_edited_linklabel")
        .attr("x", linkEditBoxX)
        .attr("y", linkEditBoxY)
        .attr("width", thisGraph.customEdges.label.width)
        .attr("height", thisGraph.customEdges.label.height)
        .on("mousedown",null)
        .on("mouseup",null)
        .on("mouseover",null)
        .on("mousedown",function(d){
          d3.event.stopPropagation();
        })
        .on("dblclick",function(d){
          d3.event.stopPropagation();
        })
        .on("click",function(d){
          d3.event.stopPropagation();
        })

  var fo_xhtml_content_edited_linklabel = fo_content_edited_linklabel
        .append('xhtml:div')
        .attr("class", "fo_xhtml_content_edited_linklabel")
        //Warning : using css doesn't work !
        .attr("style", "width:"+thisGraph.customEdges.label.width+"px;"
                      +"height:"+thisGraph.customEdges.label.height+"px;"
                      +"cursor:"+thisGraph.customNodes.cursor+";"
                      +"position:static;padding:10px")

  //linklabel Segment
  var  linklabel_segment = fo_xhtml_content_edited_linklabel
        .append("div")
        .attr("class", "ui raised segment")
        .attr("style", "position:static;margin:0px;padding:2px")

  //Form Segment
  var form_segment = linklabel_segment
        .append("div")
        .attr("class", "ui form top attached segment")
        .attr("style", "position:static;margin-top:0px;padding:0px")

    /*
     *
     * Description
     *
     * */

  //Node label 1 (description)
  var field_type = form_segment
        .append("div")
        .attr("class", "field")
        .attr("style", "margin:0px")

  var text_edited_linklabel = field_type
    .append("label")
    .attr("style", "margin:0;")
    .text("Predicat")

  //Node textarea
  var textarea_edited_linklabel = form_segment
    .append("textarea")
    .attr("id", "textarea_edited_linklabel")
    .attr("style", "padding:0;min-height:0;height:30px;width:170px;")
                .text(function() {
                  this.focus();
                    return edgeData["@type"];
                })


  if (thisGraph.config.debug) console.log("linkEdit end");
}

FluidGraph.prototype.addDataLink = function(sourceNode, targetNode)
{
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("addDataLink start");

  console.log("addDataLink start (sourceNode, targetNode)", sourceNode, targetNode);

  // draw link between mouseDownNode and this new node
  var sourceNodeIdentifier = sourceNode["@id"];
  var targetNodeIdentifier = targetNode["@id"];

  var sourceIndex = thisGraph.getIndexNodeFromIdentifier(thisGraph.d3Data.nodes, sourceNodeIdentifier);
  var targetIndex = thisGraph.getIndexNodeFromIdentifier(thisGraph.d3Data.nodes, targetNodeIdentifier);

  var sourceObj = thisGraph.d3Data.nodes[sourceIndex];
  var targetObj = thisGraph.d3Data.nodes[targetIndex];
  var newlink = { "index" : thisGraph.getLinkIndex(),
                  "@id" : thisGraph.customEdges.uriBase + thisGraph.getLinkIndex(),
                  "@type" : thisGraph.customEdges.predicate,
                  "source" : sourceObj,
                  "target" : targetObj,
                };

  thisGraph.d3Data.edges.push(newlink);

  console.log("addDataLink End d3Data = ", thisGraph.d3Data);

  if (thisGraph.config.debug) console.log("addDataLink end");

  return newlink;
}

FluidGraph.prototype.getLinkIndex = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getLinkIndex start");
  var nextLinkIndex = 0;

  if (thisGraph.d3Data.edges.length)
  {
    thisGraph.d3Data.edges.forEach(function (edge){
      if (edge.index >= nextLinkIndex)
        nextLinkIndex = edge.index+1;
    })
  }

  if (thisGraph.config.debug) console.log("getLinkIndex end");
  return nextLinkIndex;
}

FluidGraph.prototype.replaceSelectLinks = function(d3Edge, edgeData){
  var thisGraph = this;
  d3Edge.classed(thisGraph.consts.selectedClass, true);

  var searchLabelIndex = "#label" + edgeData.source.i + "_" + edgeData.target.i;
  d3.select(searchLabelIndex).attr("visibility", "visible");

  if (thisGraph.state.selectedLink){
    thisGraph.removeSelectFromLinks();
  }
  thisGraph.state.selectedLink = edgeData;
};

FluidGraph.prototype.removeSelectFromLinks = function(){
  var thisGraph = this;
  thisGraph.svgEdges.filter(function(link){
    return link === thisGraph.state.selectedLink;
  }).classed(thisGraph.consts.selectedClass, false);

  //Hide every edgeLabel
  thisGraph.bgElement.selectAll(".edgeLabel").attr("visibility", "hidden");

  thisGraph.state.selectedLink = null;
};

FluidGraph.prototype.linkOnMouseDown = function(d3path, d){
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("linkOnMouseDown start");

  d3.event.stopPropagation();
  thisGraph.state.mouseDownLink = d;

  if (thisGraph.state.selectedNode){
    thisGraph.removeSelectFromNode(thisGraph.state.selectedNode);
  }

  var prevEdge = thisGraph.state.selectedLink;
  if (!prevEdge || prevEdge !== d){
    thisGraph.replaceSelectLinks(d3path, d);
  } else{
    thisGraph.removeSelectFromLinks();
  }

  if (thisGraph.config.debug) console.log("linkOnMouseDown end");
}
